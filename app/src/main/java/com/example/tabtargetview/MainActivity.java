package com.example.tabtargetview;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.button),
                        "This is a Button", "For getting the result, just click it")
                        .outerCircleColor(R.color.colorAccent)
                        .drawShadow(true)
                        .tintTarget(false));
    }
}
